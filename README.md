# Schemas

JSON Schemas for Momayo. Very inspired by linked.art.

## Validation

```bash
# Validate the schemas
ajv compile -s "schemas/*.schema.json" --verbose -r "$(pwd)/schemas/*.schema.json" 

# Validate som data
ajv validate -s "schemas/Production.schema.json" -d examples/production.example.json --verbose -r "$(pwd)/schemas/*.schema.json" 

```